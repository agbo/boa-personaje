//
//  main.m
//  personaje
//
//  Created by Fernando Rodríguez Romero on 19/01/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//


@import Foundation;

#import "AGTCharacter.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        AGTCharacter *anakin = [AGTCharacter characterWithName:@"Anakin Skywalker"];
        
        NSLog(@"%@", anakin);
        
    }
    return 0;
}



