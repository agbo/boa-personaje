//
//  AGTCharacter.m
//  personaje
//
//  Created by Fernando Rodríguez Romero on 19/01/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTCharacter.h"

@implementation AGTCharacter

#pragma mark - class methods
+(instancetype)characterWithName:(NSString*) name
                             age:(int) age{
    
    return [[self alloc] initWithName:name
                                  age:age];
}

+(instancetype)characterWithName:(NSString*) name{
    
    return [[self alloc] initWithName:name];
}


#pragma mark - Init
-(id)initWithName:(NSString *) name
              age:(int) age{
    
    if (self = [super init]) {
        _name = name;
        _age = age;
    }
    return self;
    
}

-(id) initWithName:(NSString *)name{
    
    return [self initWithName:name
                          age:0];
}

#pragma mark - NSObject
-(NSString*) description{
    
    return [NSString stringWithFormat:@"<%@: %@ %d>",
            [self class], [self name], [self age]];
    
}


@end










