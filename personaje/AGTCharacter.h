//
//  AGTCharacter.h
//  personaje
//
//  Created by Fernando Rodríguez Romero on 19/01/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

@import Foundation;

@interface AGTCharacter : NSObject

@property NSString *name;
@property int age;

+(instancetype)characterWithName:(NSString*) name age:(int) age;
+(instancetype)characterWithName:(NSString*) name;



// designated
-(id)initWithName:(NSString *) name
              age:(int) age;

-(id) initWithName:(NSString *)name;






@end
